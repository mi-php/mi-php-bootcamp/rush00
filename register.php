<?php
session_start();
$_SESSION['message'] = '';
$mysqli = mysqli_connect("localhost", "root", "201517", "accounts_complete");

if ($_SERVER["REQUEST_METHOD"] == "POST")
{

    if ($_POST['password'] == $_POST['confirmpassword'])
    {
        $username = $mysqli->real_escape_string($_POST['username']);
        $email = $mysqli->real_escape_string($_POST['email']);
        $password = md5($_POST['password']);
          
        $_SESSION['username'] = $username;

        $sql = "INSERT INTO users (username, email, password,) " 
                . "VALUES ('$username', '$email', '$password')";
                
        if ($mysqli->query($sql) === true)
        {
            $_SESSION['message'] = "Registration succesful! Added $username to the database!";
                    header("location: cart.php");
        }
        else
        {
            $_SESSION['message'] = 'User could not be added to the database!';
        }
        $mysqli->close();          
    }
    else
    {
        $_SESSION['message'] = 'Two passwords do not match!';
    }
}
?>
<link rel="stylesheet" href="register.css" type="text/css">
<div class="body-content">
  <div class="module">
    <h1>Sign up for free</h1>
    <form class="form" action="register.php" method="post" enctype="multipart/form-data" autocomplete="off">
      <div class="alert alert-error"><?= $_SESSION['message'] ?></div>
      <input type="text" placeholder="User Name" name="username" required />
      <input type="email" placeholder="Email" name="email" required />
      <input type="password" placeholder="Password" name="password" autocomplete="new-password" required />
      <input type="password" placeholder="Confirm Password" name="confirmpassword" autocomplete="new-password" required />
      <input type="submit" value="Register" name="register" class="btn btn-block btn-primary" />
    </form>
  </div>
</div>