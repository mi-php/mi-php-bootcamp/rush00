<?php


    session_start();

    $username = $_POST['user'];
    $password = $_POST['pass'];
    $pass = hash('whirlpool', $_POST['pass']);

    $error = "";


    if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
        $error = "success";
        header('Location: success.php');
    } 
        
    if (isset($_POST['user']) && isset($_POST['pass'])) {
        if ($_POST['user'] && $_POST['pass'] > 5) {
            $_SESSION['loggedIn'] = true;
            header('Location: cart.php');
        } else {
            $_SESSION['loggedIn'] = false;
            $error = "Invalid username and password!";
        }
    }
?>

<html>
    <head>
        <title>Login Page</title>
    </head>
    
    <body>

        <?php echo $error; ?>
        
        <form method="post" action="login.php" name="login">
            <label for="username">Username:</label><br/>
            <input type="text" name="user" value=""><br/>
            <label for="password">Password:</label><br/>
            <input type="password" name="pass" value="" autocomplete="off"><br/>
            <input type="submit" value="Log In!">
        </form>
        <H1 style="color:#cccccc">Welcome to Shoestyles</H1> Where you SHOP and we SHIP!
        <a href='signup.php' alt=signup title='signup' >Sign up</a>
    </body>
</html>