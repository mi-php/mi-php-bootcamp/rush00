<?PHP
    require_once 'database_connection.php';

    /*
     * DATABASE
    */

    // Create database doesn't exist
    if (!mysqli_select_db($conn, 'clientsDB'))
    {
        $sql = "CREATE DATABASE clientsDB";
        if (!mysqli_query($conn, $sql))
        {
            echo "Could not create database clientsDB";
            exit;
        }

        // Use database after creating
        if (!mysqli_select_db($conn, 'clientsDB'))
        {
            echo "Can't use clientsDB";
            exit;
        }
    }

    // Use database if exist
    else if (!mysqli_select_db($conn, 'clientsDB'))
    {
        echo "Can't use clientsDB";
        exit;
    }

    /*
    * TABLE
    */

    $sql = "CREATE TABLE Clients (
        id" . " INT(6) AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(255) NOT NULL,
        passwd VARCHAR(255) NOT NULL,
        firstname VARCHAR(255) NOT NULL,
        lastname VARCHAR(255) NOT NULL,
        phonenumber VARCHAR(10) NOT NULL,
        email VARCHAR(255) NOT NULL
    )";

    $table = "SHOW TABLES LIKE 'Clients'";

    if (mysqli_query($conn, $table))
    {
        echo "Loading...";
    }
    else
    {
        if (!mysqli_query($conn, $sql))
        {
            echo "Error creating table";
            exit;
        }
    }

    /*
    * CLOSE CONNECTION
    */
    mysqli_close($conn);
?>
